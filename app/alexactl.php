<?php

define('TOPIC_PREFIX', 'alexactl');
$MQTT_HOST = getenv('MQTT_HOST');
putenv('EMAIL=' . getenv('ALEXACTL_EMAIL'));
putenv('PASSWORD=' .getenv('ALEXACTL_PASS'));
putenv('MFA_SECRET=' .getenv('ALEXACTL_MFA_SECRET'));

$c = new Mosquitto\Client;
$c->onConnect(function() use ($c) {
    $c->subscribe(TOPIC_PREFIX . '/#', 2);
});

$c->onMessage(function($message) {
    $topic = str_replace(TOPIC_PREFIX, '', $message->topic);
    printf("Got a message on topic %s with payload:\n%s\n", $message->topic, $message->payload);
    $payload = json_decode($message->payload, true);
    print_r($payload);
    var_dump($topic);
    if ($topic == '/devicelist') {
        $output = shell_exec('/app/alexa_remote_control.sh -a');
        var_dump($output);
        return;
    }
    list($empty, $device, $topic) = explode('/', $topic, 3);
    var_dump($empty);
    var_dump($device);
    var_dump($topic);
    switch ($topic) {
        case 'radio':
            $station = $payload['station'];
            $output = shell_exec("/app/alexa_remote_control.sh -d \"$device\" -r \"$station\"");
            var_dump($output);
            break;

        case 'pause':
            $output = shell_exec("/app/alexa_remote_control.sh -d \"$device\" -e pause");
            var_dump($output);
            break;

        case 'command':
            $command = $payload['command'];
            $output = shell_exec("/app/alexa_remote_control.sh -d \"$device\" -e textcommand:'$command'");
            var_dump($output);
            break;

        case 'automation':
            $routine = $payload['routine'];
            $output = shell_exec("/app/alexa_remote_control.sh -d \"$device\" -e automation:'$routine'");
            var_dump($output);
            break;

        case 'bibiundtina':
            $output = shell_exec("/app/alexa_remote_control.sh -d \"$device\" -s textcommand:'spiele Bibi und Tina Folge 1'");
            var_dump($output);
            break;

        case '/vol/up':
            if (isset($payload['val'])) {
                $val = "+" . (string)$payload['val'];
                control($LOGITECH_SERVER_HOST, ["mixer","volume", $val]);
            }
            else {
                control($LOGITECH_SERVER_HOST, ["mixer","volume", "+2"]);
            }
            break;

        case '/vol/down':
            if (isset($payload['val'])) {
                $val = "-" . (string)$payload['val'];
                control($LOGITECH_SERVER_HOST, ["mixer","volume", $val]);
            }
            else {
                control($LOGITECH_SERVER_HOST, ["mixer","volume", "-2"]);
            }
            break;

        case '/play':
            control($LOGITECH_SERVER_HOST, ["playlist","play", $payload['url']]);
            break;
    }
});
echo "Connecting to $MQTT_HOST\n";
$c->connect($MQTT_HOST);
$c->loopForever();
echo "Finished\n";